def konversiMenit(menit)
	jam = menit / 60
	tmp_menit = menit % 60;
	tmp_menit = "0#{tmp_menit}" if tmp_menit < 10 
	"#{jam}:#{tmp_menit}"
end

# TEST CASES
puts konversiMenit(63) # 1:03
puts konversiMenit(124) # 2:04
puts konversiMenit(53) # 0:53
puts konversiMenit(88) # 1:28
puts konversiMenit(120) # 2:00
